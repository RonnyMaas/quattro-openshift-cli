FROM centos:centos7
MAINTAINER The Quattro Project Team

ENV VERSION=1.4.1
ENV COMMIT_ID=3f9807a

# download en untar openshift-client-tools
RUN cd /tmp; \
    curl -LOk https://github.com/openshift/origin/releases/download/v$VERSION/openshift-origin-client-tools-v$VERSION-$COMMIT_ID-linux-64bit.tar.gz; \
    tar -zxvf openshift-origin-client-tools-v$VERSION-$COMMIT_ID-linux-64bit.tar.gz; \

# download checksums
RUN cd /tmp; \
    curl -LOk https://github.com/openshift/origin/releases/download/v$VERSION/CHECKSUM;

# perform checksum
RUN cd /tmp; \
    sha256sum -c CHECKSUM 2>&1 | grep -qs OK ; \
    if [ $? -gt 0 ]; then echo "wrong checksum for openshift-client-tools $VERSION" && exit 1; fi;

# move binary
RUN cd /tmp; \
    mv openshift-origin-client-tools-v$VERSION+$COMMIT_ID-linux-64bit/oc /usr/local/bin; \
    rm -rf /tmp/*;

WORKDIR /usr/local/bin

CMD ["oc", "version"]
